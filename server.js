"use strict";

var SETTINGS			       = require("./conf/settings");

process.env.SERVERNAME	 = SETTINGS.CONF.serverName	 || "Test";
process.env.NODE_ENV	   = SETTINGS.CONF.envMode		 || "development";
process.env.PORT		     = process.env.OPENSHIFT_NODEJS_PORT		     || 6500;
process.env.HOST		     = process.env.OPENSHIFT_NODEJS_IP 		       || "localhost";

var http                 = require('http');
var express              = require('express');
var path                 = require('path');
var cookieParser         = require('cookie-parser');
var bodyParser           = require('body-parser');
var expressSession       = require('express-session');

var app                  = express();

app.use(express.static(path.join(__dirname, '/public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(expressSession({secret: '^#brunexilibonátor$', saveUninitialized: true, resave: true}));

require("./routes/routing")(express, app);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

var server = http.createServer(app);

server.listen(process.env.PORT, function() {
	console.log(process.env.SERVERNAME + ' is running at ' + process.env.HOST + ":" + process.env.PORT);
});